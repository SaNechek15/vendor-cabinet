import { useEffect, useState } from 'react';
import './App.scss';
import Accordion from './ui-kit/accordion';
import Badge from './ui-kit/badge';
import Banner from './ui-kit/banner';
import Button from './ui-kit/button';
import Button2 from './ui-kit/button2';
import Card from './ui-kit/card';
import Chart from './ui-kit/chart';
import CheckBox from './ui-kit/checkBox';
import Chip from './ui-kit/chip';
import DateField from './ui-kit/dateField';
import DropDown from './ui-kit/dropDown';
import DropDown2 from './ui-kit/dropDown2';
import FormFieldGroup from './ui-kit/formFieldGroup';
import Link from './ui-kit/link';
import Menu from './ui-kit/menu';
import Radio from './ui-kit/radio';
import Switch from './ui-kit/switch';
import Table from './ui-kit/table';
import Table2 from './ui-kit/table2';
import Tabs from './ui-kit/tabs';
import Tabs2 from './ui-kit/tabs2';
import TextArea from './ui-kit/textArea';
import TextField from './ui-kit/textField';
import Toast from './ui-kit/toast';

import { GoogleLogin } from '@react-oauth/google';
// import { GoogleOAuthProvider } from '@react-oauth/google';
import { VK } from 'vk-openapi';
// https://oauth.vk.com/authorize
// client_id=51627813
// &redirect_uri=https://oauth.vk.com/blank.html
// &display=page
// &scope=offline
// &response_type=token
// &v=5.59

// https://oauth.vk.com/authorize?client_id=51627813&redirect_uri=https://oauth.vk.com/blank.html&display=page&scope=offline&response_type=token&v=5.59

const VK2 = window.VK;

function App() {

  VK2.init({
    apiId: 51627813,
  })

  const onChange = () => {

  }

  const [selectedTab, setSelectedTab] = useState(0)
  const [user, setUser] = useState()
  
  const wrap = {
    0: TabItem1, 
    1: TabItem2,
    2: '', 
  }

  const wrapAccordion = [
    {title: 'Title 1', subtitle: 'SubTitle 1', icon: 'home', content: AccordionItem1, link: 'Primary Link ACc'},
    {title: 'Title 2', content: AccordionItem2, button: 'Button Acc', buttonType: 'Secondary' },

    {title: 'Title 3', subtitle: 'SubTitle 3', icon: 'home', content: AccordionItem1, link: 'Primary Link ACc', linkIcon: 'info', linkType: 'Secondary'},
    {title: 'Title 4', subtitle: 'SubTitle 4', content: AccordionItem2, link: false, button: 'Button Acc', buttonIcon: 'info', buttonType: 'Secondary' },

  ]
  
  let Specified = wrap[selectedTab] ? wrap[selectedTab] : TabItem1


  // const url = 'https://oauth.vk.com/authorize?client_id=51627813&redirect_uri=https://oauth.vk.com/blank.html&display=page&scope=offline&response_type=token&v=5.59'
  // const response = await fetch('', { mode: 'no-cors', method: 'get', cache: 'no-cache' })

  const authVK = () => {

    VK2.Auth.login(

      // callback-функция, которая будет вызвана после авторизации
      function (response) {
    
        console.log('response',response)
    
        if (response.status === 'connected') { // авторизация прошла успешно
          setUser(response.session.user)
        } else if (response.status === 'not_authorized') { // пользователь авторизован в ВКонтакте, но не разрешил доступ приложению;
    
        } else if (response.status === 'unknown') { // пользователь не авторизован ВКонтакте.
    
        }
    
      },
      VK2.access.offline
    );
  }

  const unLoginVK = () => {

    VK2.Auth.logout(
      function (response) {
        // refreshPage(false);
        console.log('response',response)
      },
    );
  }

  const getPhotoVK = () => {

    // VK2.Api.call(
    //   'photos.getAll',
    //   {
    //     v: '5.73', 
    //     count: 5 // количество фотографий
    //   }, 
    //   function (r) {
    //     console.log('r.response', r, r.response);
    //     var count = r.response.count; // кол-во полученных фотографий
    //     var items = r.response.items; // массив с фотографиями
    //   }
    // );

    VK2.Api.call(
      'friends.get',
      {
        v: '5.131', 
        fields: 'photo_rec'
      }, 
      function (r) {
        console.log('r.response', r, r.response);
      }
    );
  }

  return  <div className="App">
      
            <nav className='navbar'> {/* ///////////////// */}
              <div className='navbar-container'>
                {/* type='danger' active */}
                <Button label='Отчеты' active ></Button>
                <Button label='Поиск транзакции' outlined></Button>
                <Button label='Поддержка' outlined></Button>
              </div>
            </nav>

            <br/><br/>

            <div className='-container'>

              <button 
                onClick={authVK}
                type="button" id="vkLogin"
              >
                VK
              </button>

              <button 
                onClick={unLoginVK}
                type="button" id="vkLogin"
              >
                unLoginVK
              </button>

              <button 
                onClick={getPhotoVK}
                type="button" id="vkLogin"
              >
                getPhotoVK
              </button>
              
              
            <br/><br/>

            {user && Object.values(user).map(i => <div>{i}</div>)}


            <br/><br/>

            <GoogleLogin
                onSuccess={credentialResponse => {
                  console.log(credentialResponse);
                }}
                onError={() => {
                  console.log('Login Failed');
                }}
              />



<br/><br/>
              <Badge
                type='Urgent'
                icon='info'
                iconright
                dataTooltip="Some     info   text Some info text Some info text Some info text Some info text Some     info   text Some info text Some info text Some info text Some info text"
                dataTooltipPos="Right"
              />
<br/><br/>

              <Button2
                label='Button'
                type='Destructive'
                iconright
                className='btnTooltip'
                // icon='info'
                // disabled
                dataTooltip="Some     info   text Some info text Some info text Some info text Some info text Some     info   text Some info text Some info text Some info text Some info text"
                // dataTooltipPos="Right"
              />
<br/><br/>

              <Link
                label='Primary Link'
                iconright
                type='Secondary'
                // disabled
                dataTooltip="Some     info   text Some info text Some info text Some info text Some info text Some     info   text Some info text Some info text Some info text Some info text"
                dataTooltipPos="Right"
              />
<br/><br/>

              <Toast
                icon='error'
                label='1 new message'
                // iconright
              />

<br/><br/> 

              <Chip
                options={chipOpt}
                // disabled
                // single
              />

<br/><br/> 

              <DateField
                label='Label'
                description='Description'
                placeholder='YYYY/MM/DD'
                mask='9999/99/99'
                // disabled
                // error='Error message'
              />

<br/><br/>
 
              <div style={{width: 500}}>
                <Table2
                  options={table2Data}
                />
              </div>
<br/><br/>    
              <div style={{width: 500}}>
                <Accordion
                  options={wrapAccordion}
                  multiple
                />
              </div>              

<br/><br/>   
              <Tabs2
                // size='Small'
                className='MYTAB'
                // type='Fixed'
                options={Tabs2Options}
                setSelectedTab={setSelectedTab}
                selectedTab={selectedTab}
              />

              <Specified/>

<br/><br/>

              <div style={{display: 'grid', gridTemplateColumns: 'repeat(2, 50%)'}}>
                <Chart
                  Ymax
                  options={chartOptions1}
                />
                <Chart
                  options={chartOptions2}
                />
              </div>
              

<br/><br/>
              <Card 
                shadow
                cardtitle='Card title'
                // type='Critical'
              >
                asd<br></br>
                fffff<br></br><br></br>
                111111111111111111Along with a description that follows the title.
                <br></br>
                fffffAlong with a description that follows the title.
                <br></br>
                fffffAlong with a description that follows the title.
              </Card>
              
<br/><br/>
              <br></br>?????????????????????????????????? <br></br>
              <FormFieldGroup
                type='Critical'
                containerclass={'FormFieldGroup__container'}
                shadow
              >
                <TextField
                  label='Label'
                  description='Description'
                  placeholder='Placeholder'
                  size='Large'
                  // disabled
                  // error='Error message'
                  minlength='2'
                  maxlength='8'
                />
                <TextField
                  // label='Label'
                  description='Description'
                  placeholder='Placeholder'
                  size='Large'
                  // disabled
                  error='Error message'
                  minlength='2'
                  maxlength='8'
                />
                <TextField
                  // label='Label'
                  // description='Description'
                  placeholder='Placeholder'
                  size='Large'
                  // disabled
                  // error='Error message'
                  minlength='2'
                  maxlength='8'
                />
              </FormFieldGroup>

<br/><br/>

              <Banner 
              type='Critical'
              bannerText='Along with a description that follows the title. Along with a description that follows the title.Along with a description that follows the title. Along with a description that follows the title.'
              >
              
              </Banner>

              
<br/><br/>
              
              
              <DropDown2
                label='Label'
                description='Description'
                placeholder='Choose One epta'
                icon='arrows'
                size='Large'
                options={dropDownArr2}
                // disabled
                error='Error message'
              />

<br/><br/><br/>
              <TextField
                label='Label'
                description='Description'
                placeholder='Placeholder'
                size='Large'
                // disabled
                error='Error message'
                minlength='2'
                maxlength='8'
              />
<br/><br/><br/>
              <TextArea
                label='Label'
                description='Description'
                placeholder='Placeholder'
                // disabled
                // error='Error message'
                // minlength='2'
                // maxlength='8'
                cols='3'
                rows='5'
              />


<br/><br/>    

              <Radio
                options={radioArr}
              />

<br/><br/>

              <CheckBox
                options={checkBoxArr}
              />

<br/><br/>

              <Switch
                options={switchArr}
              />

            </div>
            

            <br/><br/><br/><br/><br/><br/><br/><br/><br/>


            <main className='main main-container'>

              <div className='left-column'>
                <div className='left-column__title'>Отчёты</div>
                <div className='left-column__body'>
                  {/* <div className='left-column__body-title'>Все продукты</div> */}
                  <div className='left-column__body-menu'>
                    <Menu/>
                  </div>
                </div>
              </div>

              <div className='right-column'>

                <div className='right-column__header'>
                  <Tabs onChange={onChange} items={filterArr} style={{flex: '1 0 auto'}}></Tabs>
                  <Tabs onChange={onChange} items={viewArr}></Tabs>
                  <Button label='Экспорт в PDF' icon='document'></Button>
                </div>
                <div className='right-column__charts'>

                  <div className='right-column__chart'>
                    <div className='right-column__chart-top'>
                      <div data-num='+ 53' className='right-column-chart-top__title'>Выручка</div>
                      <div className='right-column-chart-top__row'>
                        <div className='right-column-chart-top__leftVal'>180 200 ₽</div>
                        <div className='right-column-chart-top__rightVal'>140 900</div>
                      </div>
                    </div>
                    <Chart
                      options={chartOptions1}
                      // height={500}
                      Ymax
                    />
                  </div>
                  <div className='right-column__chart'>
                    <Chart
                      options={chartOptions1}
                      // height={500}
                    />
                  </div>
                  <div className='right-column__chart'>
                    <Chart
                      options={chartOptions2}
                      // height={500}
                    />
                  </div>

                  <div className='right-column__chart'>
                    <Chart
                      options={chartOptions2}
                      height={400}
                      Ymax
                    />
                  </div>
                  <div className='right-column__chart'>
                    <Chart
                      options={chartOptions1}
                      // height={500}
                    />
                  </div>
                  <div className='right-column__chart'>
                    {/* <Chart
                      options={chartOptions2}
                      // height={500}
                    /> */}
                  </div>

                  <div className='right-column__chart'>
                    <Chart
                      options={chartOptions1}
                      // height={500}
                    />
                  </div>
                  <div className='right-column__chart'>
                    <Chart
                      options={chartOptions3}
                      height={100}
                    />
                  </div>
                  {/* <div className='right-column__chart'>
                    <Chart
                      options={chartOptions1}
                      // height={500}
                    />
                  </div> */}

                </div>
                <div className='right-column__table'>
                  <Table></Table>
                </div>



              </div>

            </main> 



          </div>;
}

const chartOptions1 = [
  {type: 'line', dataKey:"pv", stroke:"#635BFF", strokeWidth:'2', dot:false},
  // {type: 'line', dataKey:"uv", stroke:"#87919F", strokeWidth:'2', dot:false},
  {type: 'line', dataKey:"amt", stroke:"#0196ED", strokeWidth:'2', dot:false},
  // {type: 'bar', dataKey:"amt", fill:"#635BFF", maxBarSize: 10, stackId:"a", radius:[6, 6, 0, 0], dot: false},
  // {type: 'bar', dataKey:"pv", fill:"#013260", maxBarSize: 10, stackId:"a", radius:[6, 6, 0, 0], dot: false},
]

const chartOptions2 = [
  // {type: 'line', dataKey:"pv", stroke:"#635BFF", strokeWidth:'2', dot:false},
  // {type: 'line', dataKey:"uv", stroke:"#87919F", strokeWidth:'2', dot:false},
  // {type: 'line', dataKey:"amt", stroke:"#0196ED", strokeWidth:'2', dot:false},
  {type: 'bar', dataKey:"amt", fill:"#635BFF", maxBarSize: 10, stackId:"a", radius:[6, 6, 0, 0], dot: false},
  {type: 'bar', dataKey:"pv", fill:"#013260", maxBarSize: 10, stackId:"a", radius:[6, 6, 0, 0], dot: false},
]

const chartOptions3 = [
  {type: 'line', dataKey:"pv", stroke:"#635BFF", strokeWidth:'2', dot:false},
  {type: 'line', dataKey:"uv", stroke:"#87919F", strokeWidth:'2', dot:false},
  // {type: 'line', dataKey:"amt", stroke:"#0196ED", strokeWidth:'2', dot:false},
  // {type: 'bar', dataKey:"amt", fill:"#635BFF", maxBarSize: 10, stackId:"a", radius:[6, 6, 0, 0], dot: false},
  {type: 'bar', dataKey:"pv", fill:"#013260", maxBarSize: 10, stackId:"a", radius:[6, 6, 0, 0], dot: false},
]

const chipOpt = ['SucceededSucceeded', 'Refunded', 'Uncaptured', 'Failed']

const Tabs2Options = ['Home', 'Portfolio', 'About']

const table2Data = [
  {icon: 'home', title: 'Title table', subtitle: 'Subtitle ', value: 290000},
  {icon: 'home', title: 'Title table', value: 290000},
  {icon: 'plus', title: 'Title table', subtitle: 'Subtitle ', value: 290000},
  {icon: 'plus', title: 'Title table', value: 290000},
  {title: 'Title table', subtitle: 'Subtitle ', value: 290000},
  {icon: 'home', title: 'Title table', subtitle: 'Subtitle ', value: 290000},
  {title: 'Title table', value: 290000},
  {value: 290000},
  {title: 'Title table', value: 290000},
]

const radioArr = [
  {value: 'value1', label: 'value1', description: 'description1', disabled: false },
  {value: 'value2', label: 'value2', description: 'description2', disabled: false },
  {value: 'value3', label: 'value3', description: 'description3', disabled: true },
]

const checkBoxArr = [
  {value: 'value1', label: 'value1', description: 'description1', checked: true, disabled: false },
  {value: 'value2', label: 'value2', description: 'description2', checked: false, disabled: false },
  {value: 'value3', label: 'value3', description: 'description3', checked: false, disabled: true },
]

const switchArr = [
  {value: 'value1', label: 'value1', description: 'description1', checked: true, disabled: false },
  {value: 'value2', label: 'value2', description: 'description2', checked: false, size: 'Small', disabled: false },
  {value: 'value3', label: 'value3', description: 'description3', checked: false, disabled: true },
  {value: 'value4', label: 'value4', description: 'description4', checked: false, disabled: false },
]

const dropDownArr2 = [
  {title: 'Section 1', subitems: ['Available action 1', 'Available action 2', 'Available action 3']},
  {title: 'Section 2', subitems: ['Available action 1', 'Available action 2']},
]


const filterArr = [
  {type: 'button', label: 'Неделя'},
  {type: 'button', label: 'Месяц'},
  {type: 'button', label: 'Квартал'},
  {type: 'dropDown', label: 'Выбрать период', icon: 'calendar'},

]

const viewArr = [
  {type: 'button', label: 'Чарты', icon: 'fee'},
  {type: 'button', label: 'Таблица', icon: 'list'},
]


const TabItem1 = () => {

  return  <div className="tabs2__body-item">
            Home lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia sit, repellat assumenda excepturi minima debitis atque nulla quibusdam iste eligendi voluptas, obcaecati nihil necessitatibus vel illum itaque ea molestias vero! Home lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia sit, repellat assumenda excepturi minima debitis atque nulla quibusdam iste eligendi voluptas, obcaecati nihil necessitatibus vel illum itaque ea molestias vero!
          </div>
}

const TabItem2 = () => {

  return  <div className="tabs2__body-item">
            Portfolio lorem ipsum dolor sit amet, consectetur adipisicing elit. Id magni sit, enim tenetur animi eius ea, similique optio nostrum quibusdam ex, dolores dolorem. Recusandae quo molestiae modi saepe ratione numquam!
          </div>
}

const AccordionItem1 = () => {

  return  <div className="accordion__body-item">
            Home lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia sit, repellat assumenda excepturi minima debitis atque nulla quibusdam iste eligendi voluptas, obcaecati nihil necessitatibus vel illum itaque ea molestias vero! Home lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia sit, repellat assumenda excepturi minima debitis atque nulla quibusdam iste eligendi voluptas, obcaecati nihil necessitatibus vel illum itaque ea molestias vero!
          </div>
}

const AccordionItem2 = () => {

  return  <div className="accordion__body-item">
            Portfolio lorem ipsum dolor sit amet, consectetur adipisicing elit. Id magni sit, enim tenetur animi eius ea, similique optio nostrum quibusdam ex, dolores dolorem. Recusandae quo molestiae modi saepe ratione numquam!
          </div>
}


export default App;
