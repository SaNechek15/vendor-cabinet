import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

import { GoogleOAuthProvider } from '@react-oauth/google';
const root = ReactDOM.createRoot(document.getElementById('root'));


root.render(

  <GoogleOAuthProvider clientId="483614741937-kg3fi8ti4b01mn2uc8ae5gv4lo88d3pc.apps.googleusercontent.com">
    <App />
  </GoogleOAuthProvider>

);
