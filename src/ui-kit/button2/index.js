import React from 'react'
import Icon from '../icons'

import './style.scss'

const Button2 = ({ type='Primary', icon, onClick=()=>{}, disabled=false, className='', label='', iconright=false, dataTooltip, dataTooltipPos }) => {

    const clickHandler = () => {
        onClick()
    }

    return  <button 
                type="button"
                className={`btn2 ${type} ${disabled ? 'btn-disabled' : ''} ${className}`} 
                onClick={disabled ? () => {} : clickHandler}
                style={icon && iconright ? {flexDirection: 'row-reverse'} : {}}
                data-tooltippos={dataTooltipPos} 
                data-tooltip={dataTooltip}
            >
                {icon && <Icon icon={icon}/>}
                <span>{label}</span>
            </button>
}

export default Button2;

