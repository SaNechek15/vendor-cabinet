import React, { useRef, useState } from 'react'
import Icon from '../icons'
import { useClickOutside } from '../utils'

import './style.scss'

const Chip = ({ icon='plus', onClick=()=>{}, single=false, disabled=false, placeholder='Status', className='', options=[]}) => {

    const chipRef = useRef(null)
    const nodeRef = useRef(null)

    const [activeChip, setActiveChip] = useState(false)
    const [selectedChips, setSelectedChips] = useState([])

    useClickOutside(activeChip, nodeRef, () => setActiveChip(false))

    const clickChipHandler = (e) => {
        onClick()
        setActiveChip(!activeChip)
    }

    const clickItemHandler = (item) => {
        if(single) {
            setSelectedChips([item])
        } else {
            setSelectedChips(prev => (prev.concat(item)));
        }

        chipRef.current.click()
    }

    const clickIconHandler = () => {
        
        if (selectedChips.length) {
            setSelectedChips([])
        }
        setTimeout(() => {setActiveChip(false)}); ////////////////////////
    }

    return  <div ref={nodeRef} className={`chip ${className} ${disabled ? 'chip-disabled' : ''}`}>

                <div ref={chipRef} className={`chip__btn ${selectedChips.length ? 'selected' : ''} ${activeChip ? 'focused' : ''}`} onClick={disabled ? null : clickChipHandler}>

                    <div onClick={clickIconHandler} className='chip__icon'><Icon icon={icon}/></div>
                    <span className='chip__placeholder'>{placeholder}</span>

                    {selectedChips.length ? 
                        <>
                            <div className='chip__stick'/>
                            {selectedChips.map((item, index) => {
                                return <p key={index} className='chip__title'>{item}</p>
                            })}
                        </>
                    : null}
        
                </div>

                <div className={`chip__list ${activeChip ? 'show' : ''}`}>
                    {options.filter(f => !selectedChips.some(s => s == f)).map((item, index) => <div className='chip__list-item' onClick={() => clickItemHandler(item)} key={index}>{item}</div>)}
                </div>

            </div>
}

export default Chip;

