import React, { useEffect } from 'react'
import './style.scss'

const Tabs2 = ({ className, size='Medium', type='Fitted', setSelectedTab, selectedTab, options=[]}) => {

    const tabItems = options.map((item, index) => <span key={index} className={`tabs2__header-item ${selectedTab == index ? 'active' : ''}`} onClick={() => setSelectedTab(index)}>{item}</span>)

    return  <div className={`tabs2__header ${size} ${type} ${className}`}>
                { tabItems }
            </div>
}

export default Tabs2;