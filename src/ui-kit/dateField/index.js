import React, { useEffect, useState } from 'react'
import InputMask from 'react-input-mask';
import './style.scss'


const DateField = ({ onClick=()=>{}, disabled=false, label='', description='', placeholder='Input date', className='', mask='9999/99/99' }) => {

    const [error, setError] = useState();
    const [date, setDate] = useState();

    const changeHandler = (e) => {
        setDate(e.target.value)
    }

    const regCheck = () => {
        const minYear = mask.length == 10 ? 1900 : 0; 
        const maxYear = mask.length == 10 ? 2100 : 99; ; 
        const regex = new RegExp("^([0-9]{2,4})(-|\/|.)([0-9]{2})(-|\/|.)([0-9]{2})$");
        // const regex = new RegExp("^([0-9]{2})\\/([0-9]{2})\\/([1-2][0-9]{3})$");

        if(regex.exec(date)) {
            const reRes = regex.exec(date);
            const year = parseInt(reRes[1], 10);
            const month = parseInt(reRes[3], 10);
            const day = parseInt(reRes[5], 10);

            if(day > 31 || month > 12 || year > maxYear || year < minYear) {
                setError('Некорректный формат даты')
            } else {
                setError(null)
            }
        } else {
            setError('Неполный ввод')
        }
    }

    return  <div className={`dateField ${className}`} style={error ? {paddingBottom: 25} : {}}>

                {label || description ? 
                    <div className='dateField__top'>
                        {label ? <span className='dateField__top-label'>{label}</span> : null}
                        {description ? <span className='dateField__top-desc'>{description}</span> : null}
                    </div>
                : null}

                <label className={`dateField__item`}>
                    {/* value={value} */}
                    {/* <input type='text' maxLength="10" placeholder={placeholder} disabled={disabled} className={`dateField__item-input`} name="text" style={error ? {borderColor: '#DF1B41'} : {}} onKeyDown={onKeyPress}></input> */}
                    <InputMask 
                        placeholder={placeholder} 
                        disabled={disabled} 
                        className={`dateField__item-input`} 
                        mask={mask}
                        maskChar="_"
                        // name="text" 
                        style={error ? {borderColor: '#DF1B41'} : {}} 
                        onChange={changeHandler} 
                        onBlur={regCheck}
                    />
                    <div className={`dateField__item-error ${error ? 'show' : ''}`}>{error}</div>
                </label>

            </div>
}

export default DateField;

