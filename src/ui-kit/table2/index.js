import React from 'react'
import Icon from '../icons'

import './style.scss'

const Table2 = ({ onClick=()=>{}, options=[], className='' }) => {

    const clickHandler = () => {
        onClick()
    }

    return  <div className={`table2 ${className}`}>

                {options.map((item, index) => {

                    return  <div key={index} className='table2__row' onClick={clickHandler}>

                                {item.icon ? <div className='table2__row-icon'><Icon icon={item.icon}/></div> : null}

                                {item.title || item.title ? <div className='table2__row-body'>
                                    <div className='table2__row-body-title'>{item.title}</div>
                                    <div className='table2__row-body-subtitle'>{item.subtitle}</div>
                                </div> : null}

                                <div className='table2__row-value'>{item.value}</div>
                            </div>                    
                })}
            </div>
}

export default Table2;

