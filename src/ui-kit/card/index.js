import React from 'react'
import './style.scss'

const Card = ({children, innerref, id='', type='Default', className='', cardtitle='', shadow=false, containerclass='', containerstyle=''}) => {
/////////////////// width height
    return  <div ref={innerref} id={id} className={`card ${type} ${className ? className : ''} ${shadow ? 'shadow' : ''}`}>
                {cardtitle ? <div className='card__title'>{cardtitle}</div> : null}
                <div 
                    className={`card__container ${containerclass} `} 
                    style={containerstyle ? containerstyle : {}}
                >
                    { children }
                </div>
            </div>
}

export default Card;