import React from 'react'
import Icon from '../icons'

import './style.scss'

const Table = ({ onClick=()=>{} }) => {

    const clickHandler = () => {
        onClick()
    }

    return  <div className='table'>
                <div className='table__header'>
                    {tableHeader.map((item, index) => <div className='table__header-item' key={index}>{item}</div>)}
                </div>


                {tableData.map((item, index) => {
                    return  <div key={index} className='table__row'>
                                <div className='table__row-checkbox'></div>
                                <div className='table__row-date'>{item.date}</div>
                                <div className='table__row-seller'>
                                    <div className='table__row-sellerIcon'><Icon icon={item.sellerIcon}></Icon></div>
                                    <div className='table__row-sellerTitle'>{item.sellerTitle}</div>
                                </div>
                                <div className='table__row-sale'>{item.sale.toLocaleString()}</div>
                                <div className='table__row-revenue'>{item.revenue.toLocaleString()}</div>
                                <div className='table__row-return'>{item.return}</div>
                                <div className='table__row-extra'>{item.extra}</div>
                            </div>                    
                })}
            </div>
}

const tableHeader = ['box', 'Дата', 'Селлер', 'Количество продаж', 'Выручка', 'Возвраты', '']

const tableData = [
    {date: '23 marta (cht)', sellerIcon: 'magnit', sellerTitle: 'Магнит', sale: 58011, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'detMir', sellerTitle: 'Детский Мир', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'five', sellerTitle: 'Пятёрочка', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'magnit', sellerTitle: 'Магнит', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'detMir', sellerTitle: 'Детский Мир', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'magnit', sellerTitle: 'Магнит', sale: 58011, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'detMir', sellerTitle: 'Детский Мир', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'five', sellerTitle: 'Пятёрочка', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'magnit', sellerTitle: 'Магнит', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'detMir', sellerTitle: 'Детский Мир', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'magnit', sellerTitle: 'Магнит', sale: 58011, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'detMir', sellerTitle: 'Детский Мир', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'five', sellerTitle: 'Пятёрочка', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'magnit', sellerTitle: 'Магнит', sale: 580, revenue: 290000, return: 5, extra: '...'},
    {date: '23 marta (cht)', sellerIcon: 'detMir', sellerTitle: 'Детский Мир', sale: 580, revenue: 290000, return: 5, extra: '...'},

]

export default Table;

