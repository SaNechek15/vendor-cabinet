import React, { useEffect } from 'react'

const Tooltip = ({ target, text='Tooltip text' }) => {

    
    useEffect(() => {
        const tooltipTarget = document.querySelector(`.${target}`)
        const tooltipText = text
        let tooltipElem;
        // console.log('tooltipTarget', tooltipTarget);

        tooltipElem = document.createElement('div');
        tooltipElem.className = 'tooltip';
        tooltipElem.innerHTML = tooltipText;
        document.body.append(tooltipElem);

        let coords = tooltipTarget.getBoundingClientRect();
        let left = coords.left + (tooltipTarget.offsetWidth - tooltipElem.offsetWidth) / 2;
        if (left < 0) left = 0;

        let top = coords.top - tooltipElem.offsetHeight - 5;
        if (top < 0) {
          top = coords.top + tooltipTarget.offsetHeight + 5;
        }

        tooltipElem.style.left = left + 'px';
        tooltipElem.style.top = top + 'px';

        document.onmouseout = function(e) {

            if (tooltipElem) {
              tooltipElem.remove();
              tooltipElem = null;
            }
      
        };

    })
    return  <div>
                
            </div>
}

export default Tooltip