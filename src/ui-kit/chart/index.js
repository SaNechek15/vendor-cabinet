import React from 'react';
import { ComposedChart, Line, Bar, Area, Pie, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, ReferenceLine } from 'recharts';
import './style.scss'

const Chart = ({children, className='', Ymax, options=[], height=250}) => {

    var dataMax = Math.max.apply(null, [...data.map((i) => i.uv),...data.map((i) => i.pv), ...data.map((i) => i.amt)]);
    var dataMin = Math.min.apply(null, [...data.map((i) => i.uv),...data.map((i) => i.pv), ...data.map((i) => i.amt)]);

    const CustomizedLabel = (props) => {
        const { viewBox } = props;

        return  <g className='chart__label' >
                    <text dy={viewBox.y - 2} dx={viewBox.x} >
                        {viewBox.y < 30 ? <>{dataMax / 1000}k</> : 0}
                    </text>
                </g>
    }

    const CustomizedTick = (props) => {
        const { x, y, payload } = props;
        let xTick = payload.index == 0 ? x + 30 : x - 30
        // xTick = x

        return <g className='chart__tick' transform={`translate(${xTick},${y})`}>
            <text dy={10} dx={20} textAnchor="end">
                {payload.index == 0 || payload.index == data.length - 1 ? payload.value : null}
            </text>
        </g>
    }

    const renderCustomizedTick = (props) => {
      const { x, y, index, payload } = props;
      let offsetX = 8;
      let offsetY = 0;

      let val = payload.value && (index == 0 || index == data.length - 1) ? payload.value : null

      return (
          <text
              x={x - offsetX}
              y={y + offsetY}
              textAnchor='end'
              dominantBaseline='middle'
              className='dynamics__schedule_bar-tick'
          >
              { val }
          </text>
      );
  };

    return  <div className={`chart ${className}`} style={{height: `${height}px`}}>
                <ResponsiveContainer >
                    <ComposedChart 
                        data={data}
                        margin={{ top: 15, right: 30, left: 20, bottom: 5 }}
                        barGap={1}
                        barSize={30}
                        maxBarSize={30}
                    >
                        {/* <CartesianGrid strokeDasharray="3 3" /> */}

{/* tick={<CustomizedTick />}  tick={renderCustomizedTick} */}
                        <XAxis tickLine={false} tick={<CustomizedTick />} dataKey="name" padding={{ left: 20, right: 0 }}/>
                        <YAxis hide/>

                        {Ymax && dataMax ? <ReferenceLine y={dataMax} label={<CustomizedLabel />} stroke="#D5DBE1" strokeWidth='2'/> : null}
                        <ReferenceLine y={0} label={<CustomizedLabel />} stroke="#87919F" strokeWidth='2' />

                        {options.map((item, index) => {
                            const Specified = chartEnum && chartEnum[`${item.type}`] ? chartEnum[`${item.type}`] : null;

                            return <Specified
                                        key={index}
                                        dataKey={item.dataKey}
                                        stroke={item.stroke}
                                        strokeWidth={item.strokeWidth}
                                        dot={item.dot}
                                        
                                        fill={item.fill}
                                        barSize={item.barSize}
                                        maxBarSize={item.maxBarSize}
                                        radius={item.radius}
                                        // stackId={item.stackId}
                                    />
                        })}

                    </ComposedChart>
                </ResponsiveContainer>
            </div>

}

const chartEnum = {
    line: Line,
    bar: Bar,
    area: Area,
    pie: Pie,
}

const chartOptions = [
    {type: 'line', dataKey:"pv", stroke:"#635BFF", strokeWidth:'2', dot:false},
    // {type: 'line', dataKey:"uv", stroke:"#87919F", strokeWidth:'2', dot:false},
    {type: 'line', dataKey:"amt", stroke:"#0196ED", strokeWidth:'2', dot:false},
    // {type: 'bar', dataKey:"amt", fill:"#635BFF", maxBarSize: 10, stackId:"a", radius:[6, 6, 0, 0], dot: false},
    // {type: 'bar', dataKey:"pv", fill:"#013260", maxBarSize: 10, stackId:"a", radius:[6, 6, 0, 0], dot: false},
]

const data = [
    {
      name: 'Midnight',
      uv: 4000,
      pv: 2400,
      amt: 6400,
    },
    
    {
      name: '2',
      uv: 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: '3',
      uv: 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: '4',
      uv: 2780,
      pv: 3908,
      amt: 7000,
    },
    {
      name: '5',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: '6',
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },
    {
      name: '7',
      uv: 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: '8',
      uv: 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: '9',
      uv: 2780,
      pv: 3908,
      amt: 7000,
    },
    {
      name: '10',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: '11',
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },
    
    {
      name: '12',
      uv: 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: '13',
      uv: 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: '14',
      uv: 2780,
      pv: 3908,
      amt: 7000,
    },
    {
      name: '15',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: '16',
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },
    {
      name: '17',
      uv: 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: '18',
      uv: 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: '19',
      uv: 2780,
      pv: 3908,
      amt: 7000,
    },
    {
      name: '20',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: '21',
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },

    
    {
      name: '11:59 pm',
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
  ];

export default Chart;