import React, { useEffect, useState } from 'react'
import Icon from '../icons'

import './style.scss'

const Badge = ({ className='', type='Neutral', icon='info', iconright=false, dataTooltip, dataTooltipPos }) => {

    return  <div data-tooltippos={dataTooltipPos} data-tooltip={dataTooltip} className={`badge ${type} ${className}`} style={iconright ? {flexDirection: 'row-reverse'} : {}}>
                {icon && <Icon icon={icon}/>}
                <span className='badge__label'>{type}</span>
            </div>
}


export default Badge;

