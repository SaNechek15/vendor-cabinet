import React, { useEffect, useState } from 'react'

import './style.scss'


const TextField = ({ size='Medium', onClick=()=>{}, disabled=false, label='', description='', placeholder='Input text', className='', error='', minlength='', maxlength='', type='text'}) => {

    return  <div className={`textField ${className}`} style={error ? {paddingBottom: 25} : {}}>

                {label || description ? 
                    <div className='textField__top'>
                        {label ? <span className='textField__top-label'>{label}</span> : null}
                        {description ? <span className='textField__top-desc'>{description}</span> : null}
                    </div>
                : null}

                <label className={`textField__item`}>
                    {/* value={value} */}
                    <input type={type} placeholder={placeholder} disabled={disabled} className={`textField__item-input ${size}`} name="text" minLength={minlength} maxLength={maxlength} style={error ? {borderColor: '#DF1B41'} : {}}></input>
                    <div className={`textField__item-error ${error ? 'show' : ''}`}>{error}</div>
                </label>

            </div>
}

export default TextField;

