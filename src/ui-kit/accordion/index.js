import React, { useRef } from 'react'
import Button2 from '../button2'

import Icon from '../icons'
import Link from '../link'

import './style.scss'

const Accordion = ({ options=[], multiple=false }) => {

    const accordionItems = options.map((item, index) => <AccordionItem key={index} item={item} index={index} multiple={multiple} />)

    return  <div className='accordion'>{accordionItems}</div>
}

const AccordionItem = ({ item, index, multiple }) => {

    const ref = useRef()

    const handleOpen = (e) => {
        
        if (!multiple && !ref.current.classList.contains('active')) {
            let elems = document.querySelectorAll('.accordion__item.active')
            if (elems) {
                elems.forEach(i => {
                    i.classList.remove('active')
                })
            }
        }
        ref.current.classList.toggle('active')
    }

    const Specified = item.content ? item.content : null

    return  <div ref={ref} key={index} className={`accordion__item`} onClick={handleOpen}>
                <div className={`accordion__item-top`} >
                    <Icon icon={'arrow'} className='accordion-item-top__arrow'/>
                    {item.icon ? <Icon icon={item.icon} className='accordion-item-top__icon'/> : null}

                    <div className='accordion-item-top__body'>
                        <div className={`accordion-item-top__title`}>{item.title}</div>
                        <div className={`accordion-item-top__subtitle`}>{item.subtitle}</div>
                    </div>

                    {item.link ? 
                        <Link
                            className='accordion-item-top__link'
                            iconright
                            label={item.link}
                            icon={item.buttonIcon}
                            type={item.linkType}
                        />
                    : item.button ? 
                        <Button2
                            className='accordion-item-top__button'
                            iconright
                            label={item.button}
                            type={item.buttonType}
                            icon={item.buttonIcon}
                        />
                    : null}

                </div>

                <div className={`accordion__item-content`}>
                    <Specified/>
                </div>
            </div> 
}

export default Accordion

