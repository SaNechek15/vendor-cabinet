import React, { useEffect, useState } from 'react'

import './style.scss'

const TextArea = ({ onClick=()=>{}, disabled=false, label='Label', description='Description', placeholder='Input text', className='', error='', minlength='', maxlength='', col='', rows=''}) => {

    return  <div className={`textArea ${className}`}>

                <div className='textArea__top'>
                    <span className='textArea__top-label'>{label}</span>
                    <span className='textArea__top-desc'>{description}</span>
                </div>

                <label className={`textArea__item`}>
                    {/* value={value} */}
                    <textarea 
                        placeholder={placeholder} 
                        disabled={disabled} 
                        className={`textArea__item-input`} 
                        type="text" 
                        name="textarea" 
                        minLength={minlength} 
                        maxLength={maxlength} 
                        col={col} rows={rows} 
                        style={error ? {borderColor: '#DF1B41'} : {}}
                    />

                    <div className={`textArea__item-error ${error ? 'show' : ''}`}>{error}</div>
                </label>

            </div>
}

export default TextArea;

