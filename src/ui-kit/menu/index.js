import React, { useState } from 'react'

import './style.scss'

const Menu = () => {

    const [forceUpdate, setForceUpdate] = useState(false)

    const handleClick = (e,item) => {
        e.stopPropagation()
        item.open = !item.open
        setForceUpdate(!forceUpdate)
    }
    
    const treeNode = (data) => data.map((item, index) => {
        // const subitemsArrLength = item.subitems.length

        return  <div key={index} className='menu__item' onClick={(e) => handleClick(e, item)}>

                    <div className={`menu__title ${item.open ? 'active' : ''}`}>{item.title}</div>

                    {item.open ?
                        item.subitems && item.subitems.length ?
                        // style={{maxHeight: item.open ? `${subitemsArrLength*31}px` : 0}}
                            <div onClick={e => e.stopPropagation()} className={`menu__subitems ${item.open ? 'show' : ''}`}>
                                {item.subitems[0].title ?
                                    treeNode(item.subitems)
                                    :
                                    <>
                                        {item.subitems.map((item, index) => <div className='menu__subitem' onClick={e => e.stopPropagation()} key={index}>{item}</div>)}
                                    </>
                                }
                            </div>
                            : null
                        : null}
                </div>
    })

    return  <div className='menu'>{treeNode(itemsArr)}</div>
}

const itemsArr = [
    {title: 'Все продукты', open: true, subitems: [
        {title: 'Подписка на 1 месяц', subitems: [
            {title: 'Sub item 1', subitems: ['Детский Мир 1', 'Детский Мир 1']},
            {title: 'Sub item 2', subitems: ['Детский Мир 2']},
            {title: 'Sub item 3 +++++', subitems: [
                {title: 'Sub item 3.1', subitems: ['Магнит', 'Магнит', 'Магнит']},
            ]},
        ]},
        {title: 'Подписка на 3 месяца', subitems: [
            {title: 'Sub item 1', subitems: ['Детский Мир 1', 'Детский Мир 1']},
            {title: 'Sub item 2', subitems: ['Детский Мир 2']},
            {title: 'Sub item 3 +++++', subitems: [
                {title: 'Sub item 3.1', subitems: ['Магнит', 'Магнит', 'Магнит']},
            ]},
        ]},
    ]},

    {title: 'Other продукты', subitems: [
        {title: 'Подписка на 1 месяц', subitems: [{title: 'Подписка на 3 месяца', subitems: ['Детский Мир', 'Детский Мир', 'Детский Мир', 'Детский Мир', 'Детский Мир', 'Детский Мир']}, {title: 'Подписка на 3 месяца', subitems: ['Детский Мир', 'Детский Мир', 'Детский Мир', 'Детский Мир', 'Детский Мир', 'Детский Мир']}],},
    ]},


    // {title: 'Подписка на 1 месяц', subitems: ['Детский Мир', 'Пятёрочка', 'Магнит', 'ИП Иванов']},
    // {title: 'Подписка на 3 месяца', subitems: ['Детский Мир', 'Детский Мир', 'Детский Мир', 'Детский Мир', 'Детский Мир', 'Детский Мир']},
    // {title: 'Подписка на 6 месяцев', subitems: ['Детский Мир', 'Детский Мир', 'Детский Мир']},
]

export default Menu

