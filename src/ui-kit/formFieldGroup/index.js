import React, { useEffect, useState } from 'react'
import Card from '../card'

import './style.scss'


const FormFieldGroup = ({ children, type='Default', className='', shadow=false, containerclass }) => {

    return  <Card type={type} shadow={shadow} className={className} containerclass={containerclass}>
                {children}
            </Card>
}

export default FormFieldGroup;

