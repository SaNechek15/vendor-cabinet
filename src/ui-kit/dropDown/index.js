import React, { useEffect, useState } from 'react'
import Icon from '../icons'

import './style.scss'

const DropDown = ({ type='', icon, onClick=()=>{}, disabled=false, active, className='', label='' }) => {

    const [period, setPeriod] = useState(null)
    const [activeDropDown, setActiveDropDown] = useState(false)

    useEffect(() => {
        setActiveDropDown(active)
    }, [active])

    const clickDropDownHandler = () => {
        onClick()
        setActiveDropDown(!activeDropDown)
    }

    const clickItemHandler = (e) => {
        console.log('e', e);
        // setPeriod()
    }

    return  <div className={`btn ${type} ${active ? 'btn-active' : ''} ${disabled ? 'btn-disabled' : ''} ${className}`} onClick={disabled ? null : clickDropDownHandler}>
                    {icon && <Icon icon={icon} />}
                    <span>{label}</span>
                    <div className={`dropDown__items ${activeDropDown ? 'show' : ''}`}>
                        {dropDownArr.map((item, index) => <div onClick={(e) => clickItemHandler(e)} className='dropDown__item' key={index}>
                            {item.title}
                        </div>)}
                    </div>
            </div>
}

const dropDownArr = [
    {title: 'Неделя', value: 1},
    {title: 'Месяц', value: 2},
    {title: 'Квартал', value: 3},
]

export default DropDown;

