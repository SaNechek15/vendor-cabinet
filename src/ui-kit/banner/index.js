import React from 'react'
import Button2 from '../button2'
import Card from '../card'
import Icon from '../icons'
import './style.scss'

const Banner = ({ icon='info', type='Default', className='', bannerTitle='Notice title', bannerText='', bannerButton='Button', shadow=false }) => {

    return  <Card type={type} shadow={shadow}>
                <div className={`banner ${className} `}>
                    <Icon className='banner__icon' icon={type == 'Critical' ? 'error' : icon}/>
                    <div className='banner__body'>
                        {bannerTitle ? <div className='banner__body-title'>{bannerTitle}</div> : null}
                        <div className='banner__body-text'>{bannerText}</div>
                        <Button2 type='Secondary' className='banner__body-button' label={bannerButton}/>
                    </div>
                    <Icon className='banner__cross' icon='cross' onClick={() => {}}/>
                </div>
            </Card>
}

export default Banner;