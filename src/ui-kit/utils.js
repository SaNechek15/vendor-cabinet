import { useEffect } from "react"

export const useClickOutside = (val, ref, cb) => {
    const handleClick = e => {
        if (ref.current && !ref.current.contains(e.target)) {
            cb()
        }
    }

    useEffect(() => {
        if (val) {
            document.addEventListener('click', handleClick, { capture: true })
        } else {
        	document.removeEventListener('click', handleClick, { capture: true })
        }

        return () => {
            document.removeEventListener('click', handleClick, { capture: true })
        }
    })
}