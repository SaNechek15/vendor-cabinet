import React, { useEffect, useState } from 'react'

import './style.scss'

const Switch = ({ onClick=()=>{}, className='', options=[] }) => {

    return  <form className={`switch ${className}`}>
                {options.map((item, index) => {

                    return  <label key={index} className="switch__item">
                                <input disabled={item.disabled} className={`switch__item-input ${item.size}`} type="checkbox" name="switch" value={item.value}></input>
                                <div className='switch__item-body'>
                                    <div className="switch__item-label">{item.label}</div>
                                    <div className="switch__item-description">{item.description}</div>
                                </div>
                            </label>
                })}
            </form>
}

export default Switch;

