import React from 'react'

import './style.scss'

const Icon = ({ icon, className='' }) => {

    return  <img className={`${className}`} src={require(`./icons/${icon}.svg`)} alt="btn-icon"/>
}

export default Icon
