import React from 'react'
import Icon from '../icons'

import './style.scss'

const Link = ({ type='Primary', icon, onClick=()=>{}, disabled=false, className='', label='', iconright=false, dataTooltip, dataTooltipPos }) => {

    const clickHandler = () => {
        onClick()
    }

    return  <div 
                className={`link ${type} ${disabled ? 'link-disabled' : ''} ${className}`} 
                onClick={disabled ? () => {} : clickHandler}
                style={icon && iconright ? {flexDirection: 'row-reverse'} : {}}
                data-tooltippos={dataTooltipPos} data-tooltip={dataTooltip}
            >
                {icon && <Icon icon={icon}/>}
                <span>{label}</span>
            </div>
}

export default Link;

