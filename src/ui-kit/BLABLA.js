document.addEventListener('DOMContentLoaded', () => { // Структура страницы загружена и готова к взаимодействию

   const tabs = (tabsSelector, tabsHeadSelector, tabsBodySelector, tabsCaptionSelector, tabsCaptionActiveClass, tabsContentActiveClass) => { // объявляем основную функцию tabs, которая будет принимать CSS классы и селекторы
 
     const tabs = document.querySelector(tabsSelector) // ищем на странице элемент по переданному селектору основного элемента вкладок и записываем в константу
     const head = tabs.querySelector(tabsHeadSelector) // ищем в элементе tabs элемент с кнопками по переданному селектору и записываем в константу
     const body = tabs.querySelector(tabsBodySelector) // ищем в элементе tabs элемент с контентом по переданному селектору и записываем в константу
 
     const getActiveTabName = () => { // функция для получения названия активной вкладки
       return head.querySelector(`.${tabsCaptionActiveClass}`).dataset.tab // возвращаем значение data-tab активной кнопки
     }
 
     const setActiveContent = () => { // функция для установки активного элемента контента
       if (body.querySelector(`.${tabsContentActiveClass}`)) { // если уже есть активный элемент контента
         body.querySelector(`.${tabsContentActiveClass}`).classList.remove(tabsContentActiveClass) // то скрываем его
       }
       body.querySelector(`[data-tab=${getActiveTabName()}]`).classList.add(tabsContentActiveClass) // затем ищем элемент контента, у которого значение data-tab совпадает со значением data-tab активной кнопки и отображаем его
     }
 
     // проверяем при загрузке страницы, есть ли активная вкладка
     if (!head.querySelector(`.${tabsCaptionActiveClass}`)) { // если активной вкладки нет
       head.querySelector(tabsCaptionSelector).classList.add(tabsCaptionActiveClass) // то делаем активной по-умолчанию первую вкладку
     }
 
     setActiveContent(getActiveTabName()) // устанавливаем активный элемент контента в соответствии с активной кнопкой при загрузке страницы
 
     head.addEventListener('click', e => { // при клике на элемент с кнопками
       const caption = e.target.closest(tabsCaptionSelector) // узнаем, был ли клик на кнопке
       if (!caption) return // если клик был не на кнопке, то прерываем выполнение функции
       if (caption.classList.contains(tabsCaptionActiveClass)) return // если клик был на активной кнопке, то тоже прерываем выполнение функции и ничего не делаем
 
       if (head.querySelector(`.${tabsCaptionActiveClass}`)) { // если уже есть активная кнопка
         head.querySelector(`.${tabsCaptionActiveClass}`).classList.remove(tabsCaptionActiveClass) // то удаляем ей активный класс
       }
 
       caption.classList.add(tabsCaptionActiveClass) // затем добавляем активный класс кнопке, на которой был клик
 
       setActiveContent(getActiveTabName()) // устанавливаем активный элемент контента в соответствии с активной кнопкой
     })
   }
 
   tabs('.section__tabs', '.tabs__head', '.tabs__body', '.tabs__caption', 'tabs__caption_active', 'tabs__content_active') // вызываем основную функцию tabs для синих вкладок .section__tabs
 
   tabs('.about__tabs', '.tabs__head', '.tabs__body', '.tabs__caption', 'tabs__caption_active', 'tabs__content_active') // вызываем основную функцию tabs для зелёных вкладок .about__tabs
 
 })




return  <div className='menu'>
{itemsArr.map((item, index) => {
    const subitemsLength = item.subitems.length
    return  <div key={index} className='menu__item'>
                <div className={`menu__title ${activeIndex == index ? 'active' : ''}`} onClick={() => setActiveIndex(index)}>{item.title}</div>

                <div 
                // style={{maxHeight: activeIndex == index ? subitemsLength*31 : 0}} 
                className={`menu__subitems ${activeIndex == index ? 'show' : ''}`}>
                    {item.subitems.map((item, index) => {
                    return <div className='menu__subitem' key={index}>{item}</div>
                    })}
                </div>
            </div>
})}
</div>




import * as React from 'react';
export default function App() {
 const [forceUpdate, setForceUpdate] = React.useState(false)
 const handleClick = (e,item) => {
  e.stopPropagation()
   item.open = !item.open
   setForceUpdate(!forceUpdate)
 }
 const treeNode = (data) => data.map((item, idx) => (
   <li key={idx} onClick={() => handleClick(item)} >
     {item.label}
     {item.children && item.children.length ?
         <ul style={{display: item.open ? '' : 'none'}}>{treeNode(item.children)}</ul>
     : null}
   </li>
))


 return <ul>{treeNode(propsData)}</ul>
}


const propsData = [
 { label :  'Root1' ,  children : [
    { label :  'Item 1' ,  children : [
       { label :  'Subitem 1.1' },
       { label :  'Subitem 1.2' ,  children : [
         { label :  'Item 12' ,  children : [
            { label :  'Subitem 12.1' },
            { label :  'Subitem 12.2' },
         ]}
      ]},
    ]},
    { label :  'Item 2' ,  children : [
     { label :  'Subitem 2.1' },
     { label :  'Subitem 2.2' ,  children : [
       { label :  'Item 23' ,  children : [
          { label :  'Subitem 23.1' },
          { label :  'Subitem 23.2' },
       ]}
    ]},
  ]}
 ]},
 { label :  'Root2' ,  children : [
   { label :  'Item 2' ,  children : [
      { label :  'Subitem 2.1' },
      { label :  'Subitem 2.2' ,  children : [
        { label :  'Item 22' ,  children : [
           { label :  'Subitem 22.1' },
           { label :  'Subitem 22.2' },
        ]}
     ]},
   ]}
]},
]


// preventDefault запрещает стандартное поведение элемента. Например, клик на кнопку сабмита производит отправку формы, preventDefault это прекратит. Нажатие на ссылку перенаправляет на другую страницу $('a').preventDefault(); запретит это действе. Ссылка станет просто активной подчеркнутой областью.
// stopPropagation запрещает передачу события от ребенка к родителю, то есть, если мы кликнули по вложенному диву, его родитель "не почувствует" этого нажатия.