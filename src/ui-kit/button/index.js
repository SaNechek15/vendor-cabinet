import React from 'react'
import Icon from '../icons'

import './style.scss'

const Button = ({ type='', icon, onClick=()=>{}, disabled=false, active=false, outlined=false, className='', label='' }) => {

    const clickHandler = () => {
        onClick()
    }

    return  <div 
                className={`btn ${type} ${active ? 'btn-active' : ''} ${disabled ? 'btn-disabled' : ''} ${outlined ? 'btn-outlined' : ''} ${className}`} 
                onClick={disabled ? () => {} : clickHandler}>
                    {icon && <Icon icon={icon}/>}
                    <span>{label}</span>
            </div>
}

export default Button;

