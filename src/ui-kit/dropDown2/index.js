import React, { useEffect, useState } from 'react'
import Icon from '../icons'

import './style.scss'


const DropDown2 = ({ icon='arrows', size='Medium', onClick=()=>{}, disabled=false, label='Label', description='Description', placeholder='Choose One', className='', error='', options=[]}) => {

    const [period, setPeriod] = useState(null)/////////
    const [activeDropDown, setActiveDropDown] = useState(false)


    const clickDropDownHandler = () => {
        onClick()
        setActiveDropDown(!activeDropDown)
    }

    const clickItemHandler = (e) => {
        console.log('e', e);
        // setPeriod()
    }

    return  <div className='dropDown2'>

                <div className='dropDown2__top'>
                    <span className='dropDown2__top-label'>{label}</span>
                    <span className='dropDown2__top-desc'>{description}</span>
                </div>

                <div className={`dropDown2__btn ${activeDropDown ? 'focused' : ''} ${size} ${disabled ? 'dropDown2__btn-disabled' : ''} ${className}`} onClick={disabled ? null : clickDropDownHandler} style={error ? {borderColor: '#DF1B41'} : {}}>
                    <span>{placeholder}</span>
                    {icon && <Icon icon={icon} />}
                    
                    <div className={`dropDown2__btn-items ${activeDropDown ? 'show' : ''}`}>
                        {options.map((item, index) => 
                            <div className='dropDown2__btn-item' key={index}>
                                <div className='dropDown2__btn-item-title'>{item.title}</div>
                                <div className='dropDown2__btn-item-subitems'>
                                {item.subitems.map((item, index) => <div onClick={(e) => clickItemHandler(e)} key={index}>{item}</div>)}
                                </div>
                            </div>)
                        }
                    </div>

                    <div className={`dropDown2__btn-error ${error ? 'show' : ''}`}>{error}</div>
                    
                </div>

            </div>
}

export default DropDown2;

