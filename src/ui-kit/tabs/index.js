import React, { useState } from 'react'

import Button from '../button'
import DropDown from '../dropDown'
import Calendar from '../dropDown'

import './style.scss'

const Tabs = ({ onChange, items, style }) => {

    const [filterActiveIndex, setFilterActiveIndex] = useState(0)

    const clickFilterHandler = (item, index) => {
        setFilterActiveIndex(index)
        onChange()
    }

    return  <div className='tabs' style={style}>
                {items.map((item, index) => {
                    const Specified = tabsEnum && tabsEnum[`${item.type}`] ? tabsEnum[`${item.type}`] : null;

                    return  <Specified 
                                key={index}  
                                label={item.label}
                                className={`tabs__item `}
                                active={index === filterActiveIndex}
                                icon={item.icon}
                                onClick={() => clickFilterHandler(item, index)}
                            />
                })}
            </div>
}

const tabsEnum = {
    button: Button,
    dropDown: DropDown,
    calendar: Calendar
}

export default Tabs;