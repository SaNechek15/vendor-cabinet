import React from 'react'
import Icon from '../icons'

import './style.scss'

const Toast = ({ icon, className='', label='', iconright=false }) => {

    return  <div 
                className={`toast ${className}`} 
                style={iconright ? {flexDirection: 'row-reverse'} : {}}
            >
                {icon && <Icon icon={icon}/>}
                <span>{label}</span>
            </div>
}

export default Toast;

