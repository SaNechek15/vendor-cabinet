import React, { useEffect, useState } from 'react'

import './style.scss'

const Radio = ({ onClick=()=>{}, className='', options=[] }) => {

    return  <form className={`radio ${className}`}>
                {options.map((item, index) => {

                    return  <label key={index} className="radio__item">
                                <input disabled={item.disabled} className="radio__item-input" type="radio" name="radio" value={item.value}></input>
                                <div className='radio__item-body'>
                                    <div className="radio__item-label">{item.label}</div>
                                    <div className="radio__item-description">{item.description}</div>
                                </div>
                            </label>
                })}
            </form>
}

export default Radio;

