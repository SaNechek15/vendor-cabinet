import React, { useEffect, useState } from 'react'

import './style.scss'

const CheckBox = ({ onClick=()=>{}, className='', options=[] }) => {

    return  <form className={`checkBox ${className}`}>
                {options.map((item, index) => {
                    return  <label key={index} className="checkBox__item">
                                {/* checked={item.checked}  */}
                                <input disabled={item.disabled} className="checkBox__item-input" type="checkbox" name="checkbox" value={item.value}></input>
                                <div className='checkBox__item-body'>
                                    <div className="checkBox__item-label">{item.label}</div>
                                    <div className="checkBox__item-description">{item.description}</div>
                                </div>
                            </label>
                })}
            </form>
}

export default CheckBox;

